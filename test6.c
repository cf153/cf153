#include<stdio.h>
int input();
float average(int[],int);
void display(float);
int main()
{
    int i,n;
    float avg;
    printf("ENTER THE SIZE OF THE ARRAY:\n");
    n=input();
    int arr[n];
    printf("ENTER THE ARRAY ELEMENTS:\n");
    for(i=0;i<n;i++)
    {
        arr[i]= input();
    }
    avg = average(arr,n);
    display(avg);
    return 0;
}

 int  input()
 {
     int a;
     scanf("%d",&a);
     return a;
 }
 
 float average(int arr[],int n)
 {
    float avg,s=0;
    int i;
    for(i=0;i<n;i++)
    {
        s=s+arr[i];
    }
    avg = (float)s/n;
    return avg;
 }
 
 void display(float avg)
  {
   printf("AVERAGE OF ARRAY ELEMENTS:=%f\n",avg);
  }
 