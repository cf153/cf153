#include<stdio.h>
#include<math.h>
int main()
{
printf("ENTER THE COEFFICIENTS OF QUADRATIC EQUATION:\n");
float a,b,d,c,r1,s1,r2;
scanf("%f%f%f",&a,&b,&c);
d= b*b-4*a*c;
if(a==0)
{
printf("ROOTS CANNOT BE DETERMINED\n");
}   
else if(d>0)
{
printf("THE ROOTS ARE REAL AND DISTINCT:\n");
r1 = (-b+sqrt(d))/(2.0*a);
r2 = (-b-sqrt(d))/(2.0*a);
printf("%f\n",r1);
printf("%f\n",r2);
}
else if(d==0)
{
r1 = (-b)/(2.0*a);
printf("THE ROOTS ARE EQUAL\n");
printf("%f\n",r1);
}
else
{
printf("THE ROOTS ARE IMAGINARY:\n");
s1 = -b/(2.0*a);
r1 = sqrt(fabs(d))/(2.0*a);
printf("%f+%fi\n",s1,r1);
printf("%f-%fi\n",s1,r1);
}
}

